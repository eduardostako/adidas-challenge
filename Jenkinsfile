pipeline {
    agent {
        docker {
            image 'maven:3.8.1-adoptopenjdk-11'
            args '-v $HOME/.m2:/root/.m2'
            label 'docker'
        }
    }
    environment{
        urlPrivate = 'remoteRepo'
        imagePrivate = 'simplejava'
        tag = 'version'
    }
    stages {
        stage('Checkout config repository') {
            steps {
                checkout scm
            }
        }
        stage('SonarQube analysis') {
            steps{
                withSonarQubeEnv('MySonarQube') {
                    sh 'mvn clean package sonar:sonar'
                }
            }
        }
        stage("Quality Gate") {
            when {
                not {
                    branch 'feature/**'
                    branch 'hotfix/**'
                }
            }
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Build') {
            when {
                anyOf{
                    branch 'feature/**'
                    branch 'hotfix/**'
                }
                
            }
            steps {
                sh "mvn -Dmaven.test.failure.ignore=false clean package"
            }
            post {
                success {
                    junit '**/target/surefire-reports/*.xml'
                }
            }
        }
        stage('Build image'){
            when {
                anyOf{
                    branch 'develop'
                    branch 'staging'
                    branch 'production'
                }
                
            }
            agent {
                node {
                    label 'docker'
                }
            }
            steps {
                sh "docker build -t '${imagePrivate}':'${tag}' ."
                sh "docker tag '${imagePrivate}':'${tag}' '${urlPrivate}'/'${imagePrivate}'':'${tag}'"

                aqua customFlags: '', hideBase: false, hostedImage: '', localImage: "${imagePrivate}:${tag}", locationType: 'hosted', notCompliesCmd: '', onDisallowed: 'fail', policies: '', register: false, registry: '', showNegligible: false

                withDockerRegistry([credentialsId: privateRegistryCredentialId, url:"https://"+"${urlPrivate}"]) {
                    sh "docker push ${urlPrivate}/${imagePrivate}:${tag}"
                    script {
                        if (env.BRANCH_NAME == 'production') {
                            sh "docker push ${urlPrivate}/${imagePrivate}:stable"
                        }
                    }
                }   
            }
        }
        stage('Deploy'){
            when {
                anyOf{
                    branch 'develop'
                    branch 'staging'
                    branch 'production'
                }
                
            }
            agent {
                docker { 
                    image 'alpine/helm:3.2.4'
                    label 'docker'
                    args '-i --entrypoint='
                }
            }
            steps {
                sh "helm repo add javaApp https://${urlPrivate}'/'${imagePrivate}'':'${requiredTag}'"
                sh "helm -f values.yaml upgrade -i javaApp javaApp/javaApp --namespace 'env.BRANCH_NAME'"
            }
        }
}

