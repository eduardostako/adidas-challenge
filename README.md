# Adidas Challenge
## Pre-requirements
In order to test to an extent I feel confortable with my pipeline and all the the other configuration I am presenting here I have used the following tools:   

* Aws. Everything has been deployed using existing resources there.  

* Kops, to deploy a minimal Kubernetes cluster in EC2 instances, one master, two to three workers in a autoscaling group, in one AZ.  

* Inside Kubernetes:  
    * A Prometheus stack managed by Prometheus Operator, with node exporters, Alertmanager, Grafana  
    * Jenkins, with Docker in the agent. Also a SonarQube server  
    
* Github, to push the images.  

## Pipeline

Our project lifecycle will be based on gitflow. I am assuming that several people will be working on the project, with SCRUM or other agile methodologies an constant and fast cicles of deployment.  

For that we will have 3 main branches, develop, staging, and master, for each step of stability. Developers will work on feature/ branches and fixes done over the main branches will have the prefix of hotfix/.  

To reflect that I have used a multibranch declarative pipeline, with steps that will be execute depending of the branch.   

The steps are as follows:  

1. Checkout the code. Simple stage to bring the changes from our branch  
2. Sonarqube stage. We want to scanner the code for every branch to assure quality in the code.  
3. Quality gate stage. This stage will stop the pipeline if the quality standards for the project are not met. This will only be applied for non-developer branches as it could take time. Peer reviews for feature branches should be the way to discover those errors.  
4. Build stage. We build the application and pass the tests. If they don't pass, the pipeline will stop. As an additional step, we can save the artifact in a repository. This step will only be execute for feature and hotfix branches, as the docker build in the following step will build it again.  
5. Build image. This is the most complex stage. It will build the image using the dockerfile, tag it and if the image is for production, tag it as stable. After that, it will scanner the image. I have used Aqua, but Trivy or Sysdig Secure can be used too. If the scanner is succesful, it will push the image to our repository.  
6. Deploy stage. We deploy the application using helm, inside an docker agent with the image. First we add the repo that we have in a repository and then it upgrades the deployment in the selected branch.  

As I said, this pipeline will be multibranch, allowing all branches to be in the same place.   

All jobs regarding feature and hotfix branches will trigger as soon as there is a push to the remote branch. Every change done, will give us insight about the status of the code or the tests.  

For the development branch, the job will trigger once a pull request from a feature branch has been approved and merged.  

Staging will be something done periodically or manually, and will merge the changes from development. We can start tagging the releases here.  

Last, production job will be trigger manually and it will tag the release.  

## Monitoring

Allowing the team to react quickly to any event is a main key to fullfil our SLOs and improve SLIs.   

In this case, I have deployed a Prometheus using Prometheus operator since is the best tool for monitoring in Kubernetes environments. I have also added a jmx exporter to the application in order to measure any metric from the JVM. Using prometheus operator allows us to discover targets just adding a ServiceMonitor. It makes easier to scrape any target or not given our needs, or drop any metric that is not useful just changing a configuration. Setting up a blackbox exporter to know the status of any API is also advisable.  

We can also use alertmanager, to create alerts for our applications, for example, to monitor the heap or the 4xx requests. With a good alerting, we can react as soon as something happen or even more, react before something happen.  

Grafana will also help visualising the metrics and it even provides simple alerting.  

Also, using a logging platform such as Elastic will make easier for the developers to discover errors in the logs, allowing fastest recovering times. And last, it is also important to know the status of our request through all the components, knowing response times, error codes, ... For that we can set up a Jaeger, the opensource and opentracing compilant project, that allows to trace our request around the cluster.  

## HA

In order to accomplish High Availability in our application I have added two things to the original chart.  

* An HorizontalPodAutoscaler pointing to our deployment. This allows to scale the replicas given several metrics such as CPU usage or memory. In the example provided, the deployment will scale the pods up to 5 if the memory usage is over the 80%. Furthermore, custom metrics can be enabled, for example, we want to scale up the replcias if we are receiving more request than normal.  

* A PodAntiAffinity to assure we are not deploying all the replicas in the same node, or Availability Zone. That way our application will be available eventhough one AZ is unavailable or a node is down. In my example I have set the antiaffinity to use as topology key the zone, if a pod is already deployed in a zone, it will try to choose other. Take into account that since we normally we have more replicas than zones, we have to use preferredDuringSchedulingIgnoredDuringExecution, which will allow the allocation of more replicas even if all zones are ocupied.  

Also, to avoid downtime during the deployments, I have set a deployment strategy that will roll the update gracefully, stopping just a percentage of the pods at a time, even allowing to temporally having more pods than we have set until the readinessProbe has return a response.  

## Security
To increase security inside the cluster I would put the application behind an ingress, with a signed TLS certificate, so all communications are HTTPS. Also, we must avoid putting any sensible infomation in plain, for what, we must use Secrets inside our cluster. Those secrets will be use as volumes or environment variables in our pods, but will not be public.  

Outside the kubernetes cluster, we need to remember to don't upload passwords, certificates, privates keys, ..., to any repository, even if it is a private one. Passwords and so on, must be storage in secure applications such as Keybase or Jenkins secrets.  
